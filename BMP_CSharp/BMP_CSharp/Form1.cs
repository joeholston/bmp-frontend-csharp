﻿/*
 Drew Hayward, Joe Holston, Noah Celuch
 December 12, 2017
 COMP 322 A
 This file implements the User Interface functionality for interacting
 with the bitmap file.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestingStenoClass;

namespace BMP_CSharp
{
    public partial class Form1 : Form
    {
        private byte[] pixelData;
        private int charsTotal;
        private int charsLeft;
        private BMP_Handler handler;

        public Form1()
        {
            handler = new BMP_Handler();
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        //decode on read click
        private void readButton_Click(object sender, EventArgs e)
        {
            Stenography stego = new Stenography((int)spinBox.Value);
            editTextBox.Text = stego.decode(pixelData);
        }

        //encode and reset text box on write click
        private void writeButton_Click(object sender, EventArgs e)
        {
            Stenography stego = new Stenography((int)spinBox.Value);
            pixelData = stego.encode(editTextBox.Text, pixelData);
            editTextBox.Text = "";
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox_Click(object sender, EventArgs e)
        {

        }

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //open file dialog
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "BMP Files|*.bmp";
            openFileDialog1.Title = "Select a BMP File";

            pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
 
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //set picture in picture box and load pixel data from bitmap
                string filename = openFileDialog1.SafeFileName.ToString();
                pictureBox.Image = Image.FromFile(openFileDialog1.FileName);
                pixelData = handler.loadBMP(openFileDialog1.FileName);
            }

            //calculate the amount of characters the user can write in
            charsLeft = (pixelData.GetLength(0)*(int)spinBox.Value)/8 - 16;
            charsTotal = charsLeft;
            countText.Text = ""+charsLeft;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //give user message about program
            string message = "Steganography Frontend for BMP Files created by Joe Holston, Noah Celuch, and Drew Hayward in C#. Program created for COMP 322 at Grove City College.";
            string title = "About";
            MessageBoxButtons okButton = MessageBoxButtons.OK;
            MessageBox.Show(message, title, okButton);
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //open file dialog
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "BMP Files|*.bmp";
            saveFileDialog.Title = "Save as a BMP File";
            
            if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //save pixel data as new bmp
                string filename = saveFileDialog.FileName.ToString();
                handler.saveBMP(pixelData, filename);
            }
        }

        private void editTextBox_TextChanged(object sender, EventArgs e)
        {
            //update characters left for user
            charsLeft = charsTotal - editTextBox.TextLength;
            countText.Text = "" + charsLeft;
        }
    }
}

﻿/*
 Drew Hayward, Joe Holston, Noah Celuch
 December 12, 2017
 COMP 322 A
 This file implements encoding and decoding functionality for hiding data 
 in a bitmap file. It uses a user specified number of bits per byte.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingStenoClass
{
    class Stenography
    {
        private int hiddenBitCount;

        public Stenography(int hiddenBitCount = 1)
        {
            this.hiddenBitCount = (hiddenBitCount > 8 ? 8 : hiddenBitCount);
        }

        public byte[] encode(string plaintext, byte[] colorBytes)
        {
            // Adds headers and footers to denote message start and end
            plaintext = '\0' + plaintext + '\0';

            int maxChars = (colorBytes.Length * hiddenBitCount) / 8;

            // Shortens the message if the given string is too long
            if (plaintext.Length > maxChars)
            {
                plaintext = plaintext.Substring(0, maxChars);
            }

            // Loops through every bit in the 
            for (int i = 0; i < plaintext.Length * 8; i++)
            {
                // Calculates the index of the current color byte and plaintext byte
                int colorIndex = i / hiddenBitCount;
                int textIndex = i / 8;

                // Calculates the shift amount and mask for the color bit
                int colorShift = i % hiddenBitCount;
                int colorMask = ~(1 << colorShift);

                // Calculates the shift amount and mask for the plaintext bit
                int textShift = i % 8;
                int textMask = (1 << textShift);

                // Grabs the bytes from their buffers
                int c = (int)colorBytes[colorIndex];
                int p = (int)plaintext[textIndex];

                // Masks out the desired position from the color and inserts the bit from the text
                c = (c & colorMask) + (((p & textMask) >> textShift) << colorShift);

                // Saves the new byte to the color buffer
                colorBytes[colorIndex] = (byte)c;
            }

            return colorBytes;
        }

        public string decode(byte[] colorBytes)
        {
            string plaintext = "";
            char readChar = ' ';

            int maxChars = (colorBytes.Length * hiddenBitCount) / 8;

            for (int i = 0; i <= maxChars * 8; i++)
            {
                // Checks for message and aborts if none found
                if (i == 8 && readChar != '\0')
                {
                    return "";
                }

                // Checks for end of message
                if (readChar == '\0' && i % 8 == 0 && i != 8)
                {
                    break;
                }

                // Save read character
                if (i % 8 == 0 && i != 0 && readChar != '\0')
                {
                    plaintext += readChar;
                }

                // Calculates the index of the current color byte and plaintext byte
                int colorIndex = i / hiddenBitCount;
                int textIndex = i / 8;

                // Calculates the shift amount and mask for the color bit
                int colorShift = i % hiddenBitCount;
                int colorMask = (1 << colorShift);

                // Calculates the shift amount and mask for the plaintext bit
                int textShift = i % 8;
                int textMask = ~(1 << textShift);

                // Grabs the bytes from their buffers
                int c = (int)colorBytes[colorIndex];

                readChar = (char)((readChar & textMask) + ((((c & colorMask) >> colorShift)) << textShift));
            }

            return plaintext;
        }

        public int HiddenBits
        {
            get { return hiddenBitCount; }
            set { hiddenBitCount = (value > 8 ? 8 : value); }
        }
    }
}

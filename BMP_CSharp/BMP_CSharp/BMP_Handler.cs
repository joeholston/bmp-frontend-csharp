﻿/*
 Drew Hayward, Joe Holston, Noah Celuch
 December 12, 2017
 COMP 322 A
 This file implements loading and saving functionality for a bitmap file
 using an array of pixel data.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace BMP_CSharp
{
    class BMP_Handler
    {
        private int width;
        private int height;

        public byte[] loadBMP(string filename)
        {
            int pixelIndex = 0;
            Bitmap bmp = new Bitmap(filename);

            width = bmp.Width;
            height = bmp.Height;

            byte[] pixelData = new byte[width * height * 3];

            //go through all pixels in bitmap
            for (int i = 0; i < width; i++)
            {
                for(int j = 0; j < height; j++)
                {
                    for (int k = 0; k < 3; k++)
                    {
                        //for each pixel set byte at index equal to 
                        //R,G, or B value of the color
                        switch (pixelIndex % 3)
                        {
                            case 0:
                                pixelData[pixelIndex] = bmp.GetPixel(i, j).R;
                                break;
                            case 1:
                                pixelData[pixelIndex] = bmp.GetPixel(i, j).G;
                                break;
                            case 2:
                                pixelData[pixelIndex] = bmp.GetPixel(i, j).B;
                                break;
                            default:
                                pixelData[pixelIndex] = bmp.GetPixel(i, j).R;
                                break;
                        }
                        pixelIndex++;
                    }
                }
            }

            return pixelData;
        }

        public void saveBMP(byte[] pixelData, string filename)
        {
            Bitmap newBmp = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            Color[] pixColors = new Color[width*height];
            int pixelDataIndex = 0;
            int pixColorIndex = 0;

            //loop for the total amount of all pixels
            for (int i = 0; i < width * height; i++)
            {
                //set color of each pixel using three bytes of data
                pixColors[i] = Color.FromArgb(pixelData[pixelDataIndex],
                    pixelData[pixelDataIndex+1],
                    pixelData[pixelDataIndex+2]);
                pixelDataIndex += 3;
            }

            for(int i = 0; i < width; i++)
            {
                for(int j = 0; j < height; j++)
                {
                    //set pixel at each index using array of colors
                    newBmp.SetPixel(i, j, pixColors[pixColorIndex]);
                    pixColorIndex++;
                }
            }

            //save bmp in file system
            newBmp.Save(filename);
        }
    }
}
